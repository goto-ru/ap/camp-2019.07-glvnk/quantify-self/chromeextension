function randomInt(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
}

function generateId(n){
    var symbols = "1234567890qwertyuiopasdfghjklzxcvbnm";
    var id_device = "";
    for(var i=0; i<n; i++){
        id_device += symbols[randomInt(0, symbols.length-1)];
    }
    return id_device;
}

///////////////////////////////////////////////////////////////////


const xhr = new XMLHttpRequest();
const url='http://195.201.111.238:8001/data';

var tablink = ""
var domain = ""
var interval;
var windowFocus = false;
var targetWindow = null;
var tabCount = 0;

function start(tab) {
  chrome.windows.getCurrent(getWindows);

  chrome.tabs.getSelected(null, getUrl);
}
function getWindows(win) {
  targetWindow = win;
  windowFocus = win.focused;
  chrome.tabs.getAllInWindow(targetWindow.id, getTabs);
}
function getTabs(tabs) {
    tabCount = tabs.length;
}
function getUrl(tab){
    tablink = tab.url;
    var url = new URL(tab.url)
    domain = url.hostname
}


function step() {
    start();

    // Configuring json
    //console.log(windowFocus)
    //console.log(tabCount);
    if(windowFocus) {
        rawData = { 
            "id_device": localStorage.getItem("id_device"), 		 
            "source": "301",	 
            "version" : "1.0.0",
            "value": [{"domain" : domain, "time" : Date.now()}],    
        };
            
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        var data = JSON.stringify(rawData);
        xhr.send(data);
        //console.log(data);
    }
}

//////////////////////////////////////////////////////////////////

chrome.runtime.onInstalled.addListener(function() {
    windowFocus = true;
    // Generating id_device and saving it to cookie
    if(!localStorage.getItem("id_device")){
        localStorage.setItem("id_device", generateId(32));
    }
    console.log(localStorage.getItem("id_device"));
    
    clearInterval(interval);
    interval = setInterval(step, 1000); 
});

chrome.windows.onCreated.addListener(function() {
    windowFocus = true;
    console.log("start");
    clearInterval(interval);
    interval = setInterval(step, 1000);
})
